/*******************************************************************************
 * Copyright (c) 2023, Mercedes-Benz Tech Innovation GmbH
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

#ifndef MANTLEAPI_COMMON_MOCK_LOGGER_H
#define MANTLEAPI_COMMON_MOCK_LOGGER_H

#include <gmock/gmock.h>

#include <iomanip>
#include <ostream>

#include "MantleAPI/Common/i_logger.h"
#include "MantleAPI/Common/log_utils.h"

namespace mantle_api
{

class MockLogger final : public ILogger
{
public:
  MOCK_METHOD(LogLevel, GetCurrentLogLevel, (), (const, noexcept, override));
  MOCK_METHOD(void, Log, (LogLevel level, std::string_view message), (noexcept, override));

  void DelegateToFake() const
  {
    auto print = [](LogLevel level, std::string_view message)
    { std::cout
          << "["
          << level  // use the ostream operator from log_utils
          << "] "
          << std::quoted(message)
          << '\n'; };

    ON_CALL(*this, Log).WillByDefault(print);

    ON_CALL(*this, GetCurrentLogLevel).WillByDefault([]()
                                                     { return LogLevel::kTrace; });
  }
};

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_MOCK_LOGGER_H
