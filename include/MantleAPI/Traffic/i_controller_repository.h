#ifndef MANTLEAPI_TRAFFIC_I_CONTROLLER_REPOSITORY_H
#define MANTLEAPI_TRAFFIC_I_CONTROLLER_REPOSITORY_H

/********************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_controller_repository.h */
//-----------------------------------------------------------------------------

#pragma once

#include "MantleAPI/Traffic/i_controller_config.h"
#include "MantleAPI/Traffic/i_controller.h"

namespace mantle_api
{
/// This interface provides CRUD functionality for controllers.
/// Controller, which is assigned to exactly one entity, controls that one entity by enforcing the behavior prescribed by 
/// the control strategies assigned to that entity.
/// Multiple controllers can be assigned to an entity, but only one controller can be activated for an entity at a time.
class IControllerRepository
{
public:
  /// Creates a new controller for the entity
  ///
  /// @param config Controller config
  /// @return newly created controller for the entity
  virtual IController& Create(std::unique_ptr<IControllerConfig> config) = 0;

  /// @deprecated Creates a new controller for the entity
  ///
  /// @param id     Unique ID of the controller
  /// @param config Controller config
  /// @return newly created controller for the entity
  [[deprecated]] virtual IController& Create(UniqueId id, std::unique_ptr<IControllerConfig> config) = 0; // deprecated

  /// Gets the controller by the given unique ID
  ///
  /// @param id Unique ID of the controller
  /// @return controller according to the given unique ID
  virtual std::optional<std::reference_wrapper<IController>> Get(UniqueId id) = 0;

  /// Checks whether the internal management container contains a controller with the given ID
  ///
  /// @param id Unique ID to be searched for
  /// @return true, if controller with given ID exists, otherwise false
  [[nodiscard]] virtual bool Contains(UniqueId id) const = 0;

  /// Deletes the controller
  ///
  /// @param id The Identifier of the controller to be deleted
  virtual void Delete(UniqueId id) = 0;

};

}  // namespace mantle_api
#endif
