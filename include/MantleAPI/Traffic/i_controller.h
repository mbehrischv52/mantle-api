#ifndef MANTLEAPI_TRAFFIC_I_CONTROLLER_H
#define MANTLEAPI_TRAFFIC_I_CONTROLLER_H

/********************************************************************************
 * Copyright (c) 2021 in-tech GmbH
 *               2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made available under the
 * terms of the Eclipse Public License 2.0 which is available at
 * http://www.eclipse.org/legal/epl-2.0.
 *
 * SPDX-License-Identifier: EPL-2.0
 ********************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_controller.h */
//-----------------------------------------------------------------------------

#pragma once

#include <optional>

#include "MantleAPI/Common/i_identifiable.h"

namespace mantle_api
{
/// Base interface for all controllers.
class IController : public IIdentifiable
{
public:
  /// Desired state of lateral domain
  enum class LateralState
  {
    kNoChange = 0,
    kActivate,
    kDeactivate
  };

  /// Desired state of longitudinal domain
  enum class LongitudinalState
  {
    kNoChange = 0,
    kActivate,
    kDeactivate
  };

  /// Change the state of a controller
  /// @param[in] lateral_state      New state of the lateral domain
  /// @param[in] longitudinal_state New state of the longituindal domain
  virtual void ChangeState(LateralState lateral_state, LongitudinalState longitudinal_state) = 0;
};

}  // namespace mantle_api
#endif
