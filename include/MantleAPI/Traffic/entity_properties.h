/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 * Copyright (c) 2022 Ansys, Inc.
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  entity_properties.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_ENTITY_PROPERTIES_H
#define MANTLEAPI_TRAFFIC_ENTITY_PROPERTIES_H

#include <MantleAPI/Common/bounding_box.h>
#include <MantleAPI/Common/floating_point_helper.h>
#include <MantleAPI/Common/spline.h>
#include <MantleAPI/Common/vector.h>

#include <limits>
#include <map>
#include <string>

namespace mantle_api
{

/// Specify the type of an entity.
enum class EntityType
{
  // Other (unspecified but known)
  kOther = 1,
  // Object is a vehicle.
  kVehicle = 2,
  // Object is a pedestrian.
  kPedestrian = 3,
  // Object is an animal.
  kAnimal = 4,
  // Object is static and does not move
  kStatic = 5
};

/// Basic properties that describe scenario entities.
struct EntityProperties
{
  virtual ~EntityProperties() = default;

  /// The three dimensional bounding box that encloses the entity
  BoundingBox bounding_box{Vec3<units::length::meter_t>{}, Dimension3{}};
  /// Type of the entity object (e.g. vehicle, pedestrian)
  EntityType type{EntityType::kOther};
  /// Definition of the model of the entity
  std::string model{};
  /// Additional properties as name value pairs
  std::map<std::string, std::string> properties{};
  /// The mass of the entity in kg
  units::mass::kilogram_t mass{};
};

/// @brief Equality comparison for EntityProperties.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs exactly equal to the values of rhs.
constexpr bool operator==(const EntityProperties& lhs, const EntityProperties& rhs) noexcept
{
  return lhs.bounding_box == rhs.bounding_box && lhs.type == rhs.type && lhs.model == rhs.model;
}

/// Specify the class of a vehicle.
enum class VehicleClass
{
  kOther = 1,  // Other (unspecified but known) type of vehicle.
  // Vehicle is a small car.
  // Definition: Hatchback car with maximum length 4 m.
  kSmall_car = 2,
  // Vehicle is a compact car.
  // Definition: Hatchback car with length between 4 and 4.5 m.
  kCompact_car = 3,
  // Vehicle is a medium car.
  // Definition: Hatchback or sedan with length between 4.5 and 5 m.
  kMedium_car = 4,
  // Vehicle is a luxury car.
  // Definition: Sedan or coupe that is longer then 5 m.
  kLuxury_car = 5,
  // Vehicle is a delivery van.
  // Definition: A delivery van.
  kDelivery_van = 6,
  // Vehicle is a heavy truck.
  kHeavy_truck = 7,
  // Vehicle is a truck with semitrailer.
  kSemitrailer = 8,
  // Vehicle is a trailer (possibly attached to another vehicle).
  kTrailer = 9,
  // Vehicle is a motorbike or moped.
  kMotorbike = 10,
  // Vehicle is a bicycle (without motor and specific lights).
  kBicycle = 11,
  // Vehicle is a bus.
  kBus = 12,
  // Vehicle is a tram.
  kTram = 13,
  // Vehicle is a train.
  kTrain = 14,
  // Vehicle is a wheelchair.
  kWheelchair = 15,
  // Vehicle type not specified properly.
  kInvalid = -1
};

/// Specify the state of the indicators of a vehicle.
enum class IndicatorState
{
  // Indicator state is unknown (must not be used in ground truth).
  kUnknown = 0,
  // Other (unspecified but known) state of indicator.
  kOther = 1,
  // Indicators are off.
  kOff = 2,
  // Left indicator is on.
  kLeft = 3,
  // Right indicator is on.
  kRight = 4,
  // Hazard/warning light, i.e. both indicators, are on.
  kWarning = 5
};

/// Specify external control of a vehicle.
enum class ExternalControlState
{
  kOff = 0,
  kFull = 1,
  kLateralOnly = 2,
  kLongitudinalOnly = 3
};

/// This struct represents the performance properties of the vehicle
struct Performance
{
  /// Maximum speed of the vehicle 
  units::velocity::meters_per_second_t max_speed{std::numeric_limits<double>::infinity()};
  /// Maximum acceleration of the vehicle
  units::acceleration::meters_per_second_squared_t max_acceleration{std::numeric_limits<double>::infinity()};
  /// Maximum deceleration of the vehicle
  units::acceleration::meters_per_second_squared_t max_deceleration{std::numeric_limits<double>::infinity()};;
  /// Maximum acceleration rate of the vehicle. If omitted then infinity is assumed
  units::jerk::meters_per_second_cubed_t max_acceleration_rate{std::numeric_limits<double>::infinity()};
  /// Maximum deceleration rate of the vehicle. If omitted then infinity is assumed
  units::jerk::meters_per_second_cubed_t max_deceleration_rate{std::numeric_limits<double>::infinity()};
};

/// @brief Equality comparison for Performance.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs almost equal to the values of rhs.
constexpr bool operator==(const Performance& lhs, const Performance& rhs) noexcept
{
  return AlmostEqual(lhs.max_speed, rhs.max_speed) &&
         AlmostEqual(lhs.max_acceleration, rhs.max_acceleration) &&
         AlmostEqual(lhs.max_deceleration, rhs.max_deceleration) &&
         AlmostEqual(lhs.max_acceleration_rate, rhs.max_acceleration_rate) &&
         AlmostEqual(lhs.max_deceleration_rate, rhs.max_deceleration_rate);
}

/// This struct represents the definition of vehicle axle
struct Axle
{
  /// Maximum steering angle which can be performed by the wheels on this axle
  units::angle::radian_t max_steering{0.0};
  /// Diameter of the wheels on this axle
  units::length::meter_t wheel_diameter{0.0};
  /// Distance of the wheels center lines at zero steering
  units::length::meter_t track_width{0.0};
  /// Position of the axle with respect to the center of vehicles bounding box
  Vec3<units::length::meter_t> bb_center_to_axle_center{};
};

/// @brief Equality comparison for Axle.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs (almost) equal to the values of rhs.
constexpr bool operator==(const Axle& lhs, const Axle& rhs) noexcept
{
  return AlmostEqual(lhs.max_steering, rhs.max_steering) &&
         AlmostEqual(lhs.wheel_diameter, rhs.wheel_diameter) &&
         AlmostEqual(lhs.track_width, rhs.track_width) &&
         lhs.bb_center_to_axle_center == rhs.bb_center_to_axle_center;
}

/// Additional properties for entity objects of type vehicle
struct VehicleProperties : public EntityProperties
{
  /// Category of the vehicle (bicycle, train,...)
  VehicleClass classification{VehicleClass::kOther};

  /// Performance properties of the vehicle
  Performance performance{};

  /// Front axle of the vehicle
  Axle front_axle{};
  /// Rear axle of the vehicle
  Axle rear_axle{};

  /// The "is_host" flag determines, if the vehicle is host or not
  bool is_host{false};
  // TODO: remove, once external control for traffic is implemented through controllers
  /// The "is_controlled_externally" flag determines, if the vehicle is controlled externally or not
  bool is_controlled_externally{false};
};

/// @brief Equality comparison for VehicleProperties.
///
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs exactly equal to the values of rhs.
constexpr bool operator==(const VehicleProperties& lhs, const VehicleProperties& rhs) noexcept
{
  return lhs.bounding_box == rhs.bounding_box && lhs.type == rhs.type && lhs.model == rhs.model &&
         lhs.classification == rhs.classification && lhs.performance == rhs.performance &&
         lhs.front_axle == rhs.front_axle && lhs.rear_axle == rhs.rear_axle &&
         lhs.is_host == rhs.is_host;
}

struct PedestrianProperties : public EntityProperties
{
};

/// Additional properties for entity objects of type static
struct StaticObjectProperties : public EntityProperties
{
  /// Amount to shift position along lane normal.
  /// It allows static objects like traffic signs to be placed at certain amount above the road.
  /// It is considered when the position of the entity is set.
  units::length::meter_t vertical_offset{0.0};
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_ENTITY_PROPERTIES_H
