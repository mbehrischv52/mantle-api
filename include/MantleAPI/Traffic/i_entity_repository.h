/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_entity_repository.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_TRAFFIC_I_ENTITY_REPOSITORY_H
#define MANTLEAPI_TRAFFIC_I_ENTITY_REPOSITORY_H

#include <MantleAPI/Traffic/entity_properties.h>
#include <MantleAPI/Traffic/i_entity.h>

#include <functional>
#include <optional>
#include <string>
#include <vector>

namespace mantle_api
{
/// This interface provides CRUD functionality for scenario entities.
class IEntityRepository
{
public:
  /// Creates a new dynamic scenario entity of vehicle type
  ///
  /// @param name       The name of the entity to be created
  /// @param properties Vehicle properties
  /// @return newly created dynamic scenario entity of vehicle type
  virtual IVehicle& Create(const std::string& name, const VehicleProperties& properties) = 0;

  /// @deprecated Creates a new dynamic scenario entity of vehicle type
  ///
  /// @param id         Unique ID of the entity
  /// @param name       The name of the entity to be created
  /// @param properties Vehicle properties
  /// @return newly created dynamic scenario entity of vehicle type
  [[deprecated]] virtual IVehicle& Create(UniqueId id, const std::string& name, const VehicleProperties& properties) = 0;

  /// Creates a new scenario entity of pedestrian type
  ///
  /// @param name       The name of the entity to be created
  /// @param properties Pedestrian properties
  /// @return newly created scenario entity of pedestrian type
  virtual IPedestrian& Create(const std::string& name, const PedestrianProperties& properties) = 0;

  /// @deprecated Creates a new scenario entity of pedestrian type
  ///
  /// @param id         Unique ID of the entity
  /// @param name       The name of the entity to be created
  /// @param properties Pedestrian properties
  /// @return newly created scenario entity of pedestrian type
  [[deprecated]] virtual IPedestrian& Create(UniqueId id, const std::string& name, const PedestrianProperties& properties) = 0;

  /// Creates a new scenario entity of static object type
  ///
  /// @param name       The name of the entity to be created
  /// @param properties Static object properties
  /// @return newly created scenario entity of static object type
  virtual IStaticObject& Create(const std::string& name, const mantle_api::StaticObjectProperties& properties) = 0;

  /// @deprecated Creates a new scenario entity of static object type
  ///
  /// @param id         Unique ID of the entity
  /// @param name       The name of the entity to be created
  /// @param properties Static object properties
  /// @return newly created scenario entity of static object type
  [[deprecated]] virtual IStaticObject& Create(UniqueId id, const std::string& name, const StaticObjectProperties& properties) = 0;

  /// Gets the host vehicle
  ///
  /// @return host vehicle
  virtual IVehicle& GetHost() = 0;

  /// Gets the entity by the given name
  ///
  /// @param name The name of the entity
  /// @return entity according to the given name
  virtual std::optional<std::reference_wrapper<IEntity>> Get(const std::string& name) = 0;

  /// Gets the const entity by the given name
  ///
  /// @param name The name of the entity
  /// @return const entity according to the given name
  [[nodiscard]] virtual std::optional<std::reference_wrapper<const IEntity>> Get(const std::string& name) const = 0;

  /// Gets the entity by the given unique ID
  ///
  /// @param id Unique ID of the entity
  /// @return entity according to the given unique ID
  virtual std::optional<std::reference_wrapper<IEntity>> Get(UniqueId id) = 0;

  /// Gets the const entity by the given unique ID
  ///
  /// @param id Unique ID of the entity
  /// @return const entity according to the given unique ID
  [[nodiscard]] virtual std::optional<std::reference_wrapper<const IEntity>> Get(UniqueId id) const = 0;

  /// Checks whether the internal management container contains an scenario entity with the given ID
  ///
  /// @param id Unique ID to be searched for
  /// @return true, if scenario entity with given ID exists, otherwise false
  [[nodiscard]] virtual bool Contains(UniqueId id) const = 0;

  /// Deletes the reference entity from the scenario
  ///
  /// @param name The name of the entity to be deleted
  virtual void Delete(const std::string& name) = 0;

  /// Deletes the reference entity from the scenario
  ///
  /// @param id The Identifier of the scenario entity to be deleted
  virtual void Delete(UniqueId id) = 0;

  /// Gets all scenario entities
  ///
  /// @returns all scenario entities
  [[nodiscard]] virtual const std::vector<std::unique_ptr<mantle_api::IEntity>>& GetEntities() const = 0;

  /// Allows an external component to react on creation of an entity, such as establishing additional references pointing to it
  ///
  /// @param callback Function to be called when creating an entity
  virtual void RegisterEntityCreatedCallback(const std::function<void(IEntity&)>& callback) = 0;

  /// Allows an external component to react on deletion of an entity, such as cleaning up invalid references pointing to it
  ///
  /// @param callback Function to be called when deleting an entity with string parameter
  virtual void RegisterEntityDeletedCallback(const std::function<void(const std::string&)>& callback) = 0;

  /// Allows an external component to react on deletion of an entity, such as cleaning up invalid references pointing to it
  ///
  /// @param callback Function to be called when deleting an entity with UniqueId parameter
  virtual void RegisterEntityDeletedCallback(const std::function<void(UniqueId)>& callback) = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_TRAFFIC_I_ENTITY_REPOSITORY_H
