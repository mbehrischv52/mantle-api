/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  i_coord_converter.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_MAP_I_COORD_CONVERTER_H
#define MANTLEAPI_MAP_I_COORD_CONVERTER_H

#include <MantleAPI/Common/position.h>
#include <MantleAPI/Common/vector.h>
#include <units.h>

namespace mantle_api
{
/// Interface that provides functionality to query the underlying map with regard to transformation of different
/// position types, curvature, elevation etc.
class ICoordConverter
{
public:
  virtual ~ICoordConverter() = default;

  /// Converts a track position to its corresponding inertial position.
  ///
  /// @param position Track position. Coordinate system is implicitly defined by underlying type of Position
  /// @return Inertial position
  [[nodiscard]] virtual Vec3<units::length::meter_t> Convert(Position position) const = 0;
};

}  // namespace mantle_api

#endif  // MANTLEAPI_MAP_I_COORD_CONVERTER_H
