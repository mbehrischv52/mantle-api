/*******************************************************************************
 * Copyright (c) 2021-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  position.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_POSITION_H
#define MANTLEAPI_COMMON_POSITION_H

#include <MantleAPI/Common/floating_point_helper.h>
#include <MantleAPI/Common/vector.h>
#include <cmath>
#include <cstdint>
#include <string>
#include <units.h>
#include <variant>

namespace mantle_api
{

/// Position defined in terms of the road coordinates (t,s) applied to a given road as defined in OpenDRIVE
struct OpenDriveRoadPosition
{
  /// @brief RoadId as defined in OpenDRIVE
  std::string road{};
  /// @brief Offset in s direction w.r.t. road, unit: [m]
  units::length::meter_t s_offset{0.0};
  /// @brief Offset in t direction w.r.t. reference line of road, unit: [m]
  units::length::meter_t t_offset{0.0};
};

/// Position that is determined by a lane (lane ID as defined in OpenDRIVE) and the s coordinate of a given road
struct OpenDriveLanePosition
{
  /// @brief RoadId as defined in OpenDRIVE
  std::string road{};
  /// @brief LaneId as defined in OpenDRIVE (e.g. -1 for right lane)
  std::int32_t lane{0};
  /// @brief Offset in s direction w.r.t lane (same as road), unit: [m]
  units::length::meter_t s_offset{0.0};
  /// @brief Offset in t direction w.r.t center line of lane, unit: [m]
  units::length::meter_t t_offset{0.0};
};

/// Position defined in terms of the spherical geographic coordinates (angular Longitude and Latitude)
struct LatLonPosition
{
  /// @brief GPS latitude, unit: [rad]
  units::angle::radian_t latitude{0.0};
  /// @brief GPS longitude, unit: [rad]
  units::angle::radian_t longitude{0.0};
};

/// Variant of possible definitions of position (e.g. in terms of the road coordinates, spherical geographic coordinates etc.)
using Position = std::variant<OpenDriveRoadPosition, OpenDriveLanePosition, LatLonPosition, Vec3<units::length::meter_t>>;

/// @brief Equality comparison for OpenDriveRoadPosition.
///
/// **Attention** Floating-point comparision may require tweaks in precision.
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs (almost) equal to the values of rhs.
inline bool operator==(const OpenDriveRoadPosition& lhs, const OpenDriveRoadPosition& rhs) noexcept
{
  return lhs.road == rhs.road &&
         AlmostEqual(lhs.s_offset, rhs.s_offset) &&
         AlmostEqual(lhs.t_offset, rhs.t_offset);
}

/// @brief  Inequality comparison for OpenDriveLanePosition.
///
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]	rhs right-hand side value for the comparison
/// @returns  true if the value of lhs is not almost equal to the value of rhs.
inline bool operator!=(const OpenDriveRoadPosition& lhs, const OpenDriveRoadPosition& rhs) noexcept
{
  return !(lhs == rhs);
}

/// @brief Equality comparison for OpenDriveLanePosition.
///
/// **Attention** Floating-point comparision may require tweaks in precision.
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs (almost) equal to the values of rhs.
inline bool operator==(const OpenDriveLanePosition& lhs, const OpenDriveLanePosition& rhs) noexcept
{
  return lhs.road == rhs.road &&
         lhs.lane == rhs.lane &&
         AlmostEqual(lhs.s_offset, rhs.s_offset) &&
         AlmostEqual(lhs.t_offset, rhs.t_offset);
}

/// @brief  Inequality comparison for OpenDriveLanePosition.
///
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]	rhs right-hand side value for the comparison
/// @returns  true if the value of lhs is not almost equal to the value of rhs.
inline bool operator!=(const OpenDriveLanePosition& lhs, const OpenDriveLanePosition& rhs) noexcept
{
  return !(lhs == rhs);
}

/// @brief Equality comparison for LatLonPosition.
///
/// **Attention** Floating-point comparision may require tweaks in precision.
/// @param[in]  lhs The left-hand side value for the comparison
/// @param[in]	rhs The right-hand side value for the comparison
/// @returns  true if the values of lhs almost equal to the values of rhs.
constexpr bool operator==(const LatLonPosition& lhs, const LatLonPosition& rhs) noexcept
{
  return AlmostEqual(lhs.latitude, rhs.latitude) && AlmostEqual(lhs.longitude, rhs.longitude);
}

/// @brief  Inequality comparison for LatLonPosition.
///
/// @param[in]  lhs left-hand side value for the comparison
/// @param[in]	rhs right-hand side value for the comparison
/// @returns  true if the value of lhs is not almost equal to the value of rhs.
constexpr bool operator!=(const LatLonPosition& lhs, const LatLonPosition& rhs) noexcept
{
  return !(lhs == rhs);
}

}  // namespace mantle_api

#endif  // MANTLEAPI_COMMON_POSITION_H
