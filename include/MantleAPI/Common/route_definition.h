/*******************************************************************************
 * Copyright (c) 2022-2023, Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  route_definition.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_COMMON_ROUTE_DEFINITION_H
#define MANTLEAPI_COMMON_ROUTE_DEFINITION_H

#include <MantleAPI/Common/vector.h>
#include <units.h>
#include <vector>

namespace mantle_api
{
/// Defines how a route should be calculated
enum class RouteStrategy
{
  kUndefined = 0,
  kFastest,
  kLeastIntersections,
  kShortest
};

/// Groups a Waypoint with a RouteStrategy
struct RouteWaypoint
{
  /// Reference position used to form a route
  mantle_api::Vec3<units::length::meter_t> waypoint{};
  /// Defines how a route should be calculated
  RouteStrategy route_strategy{RouteStrategy::kUndefined};
};

/// Serves as a raw set of global coordinates and information for
/// linking them, from which the actual route can be calculated
struct RouteDefinition
{
  /// The list of waypoints with associated RouteStrategies
  std::vector<mantle_api::RouteWaypoint> waypoints{};
};

}  // namespace mantle_api
#endif  // MANTLEAPI_COMMON_ROUTE_DEFINITION_H
