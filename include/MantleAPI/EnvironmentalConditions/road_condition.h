/*******************************************************************************
 * Copyright (c) 2021-2023 Bayerische Motoren Werke Aktiengesellschaft (BMW AG)
 *
 * This program and the accompanying materials are made
 * available under the terms of the Eclipse Public License 2.0
 * which is available at https://www.eclipse.org/legal/epl-2.0/
 *
 * SPDX-License-Identifier: EPL-2.0
 *******************************************************************************/

//-----------------------------------------------------------------------------
/** @file  road_condition.h */
//-----------------------------------------------------------------------------

#ifndef MANTLEAPI_ENVIRONMENTALCONDITIONS_ROAD_CONDITION_H
#define MANTLEAPI_ENVIRONMENTALCONDITIONS_ROAD_CONDITION_H

#include <MantleAPI/Common/position.h>
#include <units.h>

namespace mantle_api
{
/// Definition of the position of the rectangle
struct Rectangle
{
  /// The position of the lower left corner of the rectangle
  Position bottom_left{};
  /// The position of upper right corner of the rectangle
  Position top_right{};
};

/// Definition of the road friction patch
struct FrictionPatch
{
  /// Geometric properties of the road friction patch
  Rectangle bounding_box{};
  /// Percentage of road friction
  units::concentration::percent_t friction{100.0};
};
}  // namespace mantle_api
#endif  // MANTLEAPI_ENVIRONMENTALCONDITIONS_ROAD_CONDITION_H
