# MantleAPI

A collection of interfaces for abstraction between a scenario engine and an environment simulator.
It is intended to be usable with a wide variety of scenario description languages by implementing according scenario engines.

Remark: This is currently work in progress and no stable state is reached yet.

## Used libraries

### Units
License: MIT License
URL: https://github.com/nholthaus/units
Version: v2.3.3

### GoogleTest
License: BSD-3-Clause License
URL: https://github.com/google/googletest
Version: v1.12.1

### CPM
License: MIT License
URL: https://github.com/cpm-cmake/CPM.cmake
Version: v0.36.0


## Build

In order to build this project please consider the following sections. 

The used configuration for all setups is `Debug`. For release builds change the configuration to `Release`.  

### Windows wsl2 Ubuntu and on Linux/Ubuntu native (prefered)

Install Requirements:
```
sudo apt install graphviz doxygen cmake g++
```

1. Build via cmake:
    ```
    cd <path_to_mantle_api>
    cmake -B build -DCMAKE_BUILD_TYPE=Debug -DCMAKE_INSTALL_PREFIX=$PWD/install
    cmake --build build -j
    cmake --install build
    ```

2. Build doc:
    ```
    cd <path_to_mantle_api>
    cmake --build build --target MantleAPI_doc
    cat <path_to_mantle_api>/build/doc/html/index.html
    ```

3. Test via ctest:
    ```
    cd <path_to_mantle_api>/build
    ctest --stop-on-failure
    ```
    ctest runs the tests from both build targets: `CommonTests` and `MentleAPITests`.

### Windows mingw with cmake
This section introduces a step by step guide to install the requirements to build this API with mingw tooling.

Requirements:
- Follow the following guide to set up the mingw64 shell and the required dependencies (cmake, gcc, ninja, doxygen, graphviz): https://www.eclipse.org/openpass/content/html/installation_guide/20_install_prerequisites.html#installing-the-binary-packages 


Build via cmake (on a mingw console): 
1. Go to the project dir: `cd <project_dir>`
2. Build via cmake:
    ```
    mkdir build
    cd build
    cmake -G "Ninja" ..
    ninja
    ```
3. Test the API 
    ```
    cd <path_to_mantle_api>/build/test
    ./MantleAPITest.exe
    ```
4. Build Doc: 
    ```
    cd <path_to_mantle_api>/build
    cmake --build . --target MantleAPI_doc
    ```

### Windows native with cmake

This section introduces a step by step guide to install the requirements to build this API with native tooling.

Requirements:
- install cmake: https://cmake.org/install/
- install Visual Studio build tools 2022: https://visualstudio.microsoft.com/downloads/#remote-tools-for-visual-studio-2022
- install doxygen: https://www.doxygen.nl/download.html
- install graphviz: https://graphviz.org/download/

Add doxygen, graphiz, and cmake to at least the current users PATH variable.

1. Configure:
    ```
    cmake.exe --no-warn-unused-cli -DCMAKE_EXPORT_COMPILE_COMMANDS:BOOL=TRUE -DCMAKE_BUILD_TYPE:STRING=Debug "-DCMAKE_C_COMPILER:FILEPATH=C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC\Tools\Llvm\bin\clang.exe" "-DCMAKE_CXX_COMPILER:FILEPATH=C:\Program Files (x86)\Microsoft Visual Studio\2022\BuildTools\VC\Tools\Llvm\bin\clang.exe" -S<path_to_mantle_api> -B<path_to_mantle_api>/build -G Ninja
    ```

2. Build:
    ```
    cmake.exe --build <path_to_mantle_api>/build --config Debug --target all --
    ```

3. Run Tests: 

    Common Test: 
    ```
    $> ."<path_to_mantle_api>/build/CommonTest.exe"
    Running main() from gmock_main.cc
    ...
    ```

    Mantle API Test:
    ```
    $> ."<path_to_mantle_api>/build/MantleAPITest.exe"
    Running main() from gmock_main.cc
    ...
    ```

4. Build Doc:
    ```
    cmake.exe --build <path_to_mantle_api>/build --config Debug --target MantleAPI_doc --
    ```

    The generated html doc is located here: `<path_to_mantle_api>/build/doc/html/index.html`

## Usage in your own project with cmake

1. Adding it to your own CMakeLists.txt similar to:

    ```
    find_package(MantleAPI)
    if (MantleAPI_FOUND)
        add_executable(testmantle testmantle_main.cpp)
        target_link_libraries(testmantle MantleAPI::MantleAPI)
    endif()
    ```

2. Then make sure that your cmake call finds mantle-api like this:
    ```
    cmake -DCMAKE_PREFIX_PATH=<path_to_mantle_api>/install
    ```

## Developer Notes

### Versioning

Versioning should follow the semantic versioning pattern `MAJOR.MINOR.PATCH`.  
Currently this is a manual task that needs to be approved as a part of a code review.

In the following a course guideline is given, when to increment the according fields:

**Major:**

- Even minor changes to a public header will most likely result in an increase of this number (= breaking change).
  As this is probably the most common case MAJOR will therefore be incremented relatively frequently compared to other libraries.

**Minor:**

- Increments, if changes are ABI compatible, i.e. independent of the rest of other mantle definitions.  
  It is expected that this does not happen very often at the current state of development.
- Features related to testing, such as enhancing mocks, adding tests, etc.
- Features related to chore tasks, such as CI scripts, CMake enhancements, formatters, etc.

**Patch:**

- Fixing typos in the documentation (as long as they don't change the semantic of a function call!)
- Fixing bugs related to testing
- Fixing bugs related to chore tasks

